﻿#ifndef GOODSWAREHOUSING_H
#define GOODSWAREHOUSING_H

#include <QWidget>


#pragma execution_character_set("utf-8");


// 数据库相关头文件
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>

#include <QtDebug>
#include <QMessageBox>
#include <QDateTime>

// 商品入库大家可以设计数据表来存储

namespace Ui {
class GoodsWarehousing;
}

class GoodsWarehousing : public QWidget
{
    Q_OBJECT

public:
    explicit GoodsWarehousing(QWidget *parent = nullptr);
    ~GoodsWarehousing();

private slots:
    void on_pushButton_InputGoods_clicked();

    void on_pushButton_Exit_clicked();

private:
    Ui::GoodsWarehousing *ui;

public:
    void InitComboBoxFunc(); // 初始化Combo box控件
};

#endif // GOODSWAREHOUSING_H
