﻿#include "goodsdelivery.h"
#include "ui_goodsdelivery.h"

GoodsDelivery::GoodsDelivery(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GoodsDelivery)
{
    ui->setupUi(this);

    // 禁止窗口最化大按钮
    setWindowFlags(windowFlags()& ~Qt::WindowMaximizeButtonHint);

    // 禁止用户拖拉窗口改变大小
    setFixedSize(this->width(),this->height());

    // 初始化Combo Box控件（读取数据库，商品数据表中的编号显示到此控件）
    InitComboBoxFunc();

}

GoodsDelivery::~GoodsDelivery()
{
    delete ui;
}

void GoodsDelivery::on_pushButton_OutputGoods_clicked()
{
    // 获取Combo Box控件里面的值
    QString StrCBId=ui->comboBox_Id->currentText();


    // 判断商品入库的数量是否为空
    if(ui->lineEdit_Amount->text().isEmpty())
    {
        QMessageBox::critical(this,"提示","商品出库的数量不能为空，请重新检查？");
        ui->lineEdit_Amount->setFocus();
        return;
    }

    // 设计SQL查询语句条件
    // SQL查询 SELECT * FROM commoditydatatable where StockId=1001

    QSqlQuery sqlquery;
    QString strid="StockId=";
    strid+=StrCBId;

    QString str=QString("SELECT * FROM commoditydatatable where %1").arg(strid);
    sqlquery.exec(str);
    // QMessageBox::critical(this,"提示",str);

    // 获取数据表中的商品编号对应的数量
    int i=0;
    QString strAmount;
    while(sqlquery.next())
    {
        strAmount=sqlquery.value(2).toString();
        // QMessageBox::information(this,"提示",strAmount);
    }


    // 将输入数量+数量表当中的数量
    int inputamount=ui->lineEdit_Amount->text().toInt(); // 用户输入数量
    int tableamount=strAmount.toUInt(); // 数据表里面的数量转换为整型
    int isum=tableamount-inputamount; // 实现相加

    // int 转换QString
    QString strresult=QString::number(isum);

    // 更新数据表中数量字段的值
    QString strdb=QString("update commoditydatatable set stockamount=%1 where %2").arg(strresult).arg(strid);

    if(sqlquery.exec(strdb))
    {
        QMessageBox::information(this,"提示","恭喜你，商品出库成功！");
    }
    else
    {
        QMessageBox::critical(this,"提示","对不起，商品出库失败，请重新检查？");
    }

}

void GoodsDelivery::on_pushButton_Exit_clicked()
{
    close();
}


void GoodsDelivery::InitComboBoxFunc() // 初始化Combo box控件
{
    int i=0;
    QSqlQuery sqlQuery;
    sqlQuery.exec("SELECT * FROM commoditydatatable");

    QString StrId;

    while(sqlQuery.next())
    {
        StrId=sqlQuery.value(0).toString();
        ui->comboBox_Id->insertItem(i,StrId);
        i++;
    }

}
