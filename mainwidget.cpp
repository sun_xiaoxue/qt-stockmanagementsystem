﻿#include "mainwidget.h"
#include "ui_mainwidget.h"

#if _MSC_VER >= 1600
#pragma execution_character_set("utf-8");
#endif

MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWidget)
{
    ui->setupUi(this);

    // 设置主窗口背景颜色
    QPalette plt;
    plt.setColor(QPalette::Window,QColor(180,220,130));
    this->setPalette(plt);

    ConnectMySQLDBFunc();               // 连接MySQL数据库

    // 禁止窗口最大化按钮
    setWindowFlags(windowFlags()&~Qt::WindowMaximizeButtonHint);

    // 禁止用户拖拉窗口改变大小
    setFixedSize(this->width(),this->height());

    // tablewidget表格控件初始化操作
    TableWidgetListDataFunc();

    // 初始化表格控件列宽度
    InitTableWidgetFunc();


}

MainWidget::~MainWidget()
{
    delete ui;
}

void MainWidget::ConnectMySQLDBFunc()   // 连接MySQL数据库函数
{
    QSqlDatabase db=QSqlDatabase::addDatabase("QODBC");
    db.setHostName("127.0.0.1");        // 控制面板ODBC数据源配置IP保存一致
    db.setPort(3306);                   // 此端口号为：MySQL数据库安装时设置端口编号
    db.setDatabaseName("stockmsdb");    // ODBC数据源配置的名称
    db.setPassword("123456");           // 这个口令密码为：安装MySQL数据库时设置的密码

    bool bok=db.open();                 // 打开数据库

    if(!bok)                            // 加取反符号，失败才会弹出：连接失败消息框
    {
        QMessageBox::critical(this,"提示","MySQL数据库连接失败！！！");
    }
}

void MainWidget::on_pushButton_DataSearch_clicked()
{
    // 表格控件初始化
    InitTableWidgetFunc();

    // 先清除表格控件数据
    ui->tableWidget_ListData->clear();


    QSqlQuery sqlquery;
    QString strid="StockId=";
    strid=strid+ui->lineEdit_GoodsNumber->text();

    // SQL查询语句
    QString str=QString("select *from commoditydatatable where %1").arg(strid);
    sqlquery.exec(str);

    // 设置表格控件标题（表头）
    ui->tableWidget_ListData->setHorizontalHeaderLabels(QStringList()<<"编号"<<"名称"<<"数量"<<"单价"<<"供应商家"<<"负责人"<<"入库时间"<<"出库时间"<<"备注");

    int i=0;
    while(sqlquery.next())
    {
        // 将数据库里面的数据表读取出来转换成对应的字符串,赋给对应字符串变量
        QString strId=sqlquery.value(0).toString();
        QString strName=sqlquery.value(1).toString();
        QString strAmoumt=sqlquery.value(2).toString();
        QString strUnitPrice=sqlquery.value(3).toString();
        QString strSupplier=sqlquery.value(4).toString();
        QString strDirector=sqlquery.value(5).toString();
        QString strWareHouseTime=sqlquery.value(6).toString();
        QString strDeliveryTime=sqlquery.value(7).toString();
        QString strRemarks=sqlquery.value(8).toString();

        // 将字符串显示到表格控件当中
        ui->tableWidget_ListData->setItem(i,0,new QTableWidgetItem(strId));
        ui->tableWidget_ListData->setItem(i,1,new QTableWidgetItem(strName));
        ui->tableWidget_ListData->setItem(i,2,new QTableWidgetItem(strAmoumt));
        ui->tableWidget_ListData->setItem(i,3,new QTableWidgetItem(strUnitPrice));
        ui->tableWidget_ListData->setItem(i,4,new QTableWidgetItem(strSupplier));
        ui->tableWidget_ListData->setItem(i,5,new QTableWidgetItem(strDirector));
        ui->tableWidget_ListData->setItem(i,6,new QTableWidgetItem(strWareHouseTime));
        ui->tableWidget_ListData->setItem(i,7,new QTableWidgetItem(strDeliveryTime));
        ui->tableWidget_ListData->setItem(i,8,new QTableWidgetItem(strRemarks));
    }
}

// 调用：新增商品对话框
#include "addcommodity.h"
void MainWidget::on_pushButton_AddGoods_clicked()
{
    AddCommodity *adddlg=new AddCommodity();
    adddlg->show();
}

// 删除商品
void MainWidget::on_pushButton_DeleteGoods_clicked()
{
    // 1：首先：提醒用户是否确认删除此记录
    // 1：获取QMessageBox选择按钮：Yes 或 No
    int iClick=QMessageBox::warning(this,"警告","警告：删除操作是根据商品编号进行删除，请确认是否安全？",QMessageBox::Yes|QMessageBox::No);

    // 2：判断用户是否单击Yes按钮
    if(iClick==QMessageBox::Yes)
    {
        // 3：获取当前选择行进行删除：商品记录数据
        int iRow=ui->tableWidget_ListData->currentRow();

        // 4：根据index（行，列）获取对应商品编号
        QString strValue=ui->tableWidget_ListData->model()->index(iRow,0).data().toString();
        // QMessageBox::information(this,"测试","获取数据为:"+strValue);

        QSqlQuery result;
        QString strid=strValue;

        // 5：判断删除商品编号是否为空？？？
        if(strid==NULL)
        {
            QMessageBox::critical(this,"错误","提示：请选择要删除商品编号，请重新检查？");
            return;
        }


        // 6：实现删除
        QString sqlquery=QString("delete from commoditydatatable where StockId = %1").arg(strid);
        if(result.exec(sqlquery))
        {
            // 删除提示
            QMessageBox::information(this,"提示","删除商品记录成功！");

            // 清空表格控件数据
            ui->tableWidget_ListData->clear();

            // 设置表格控件标题（表头）
            ui->tableWidget_ListData->setHorizontalHeaderLabels(QStringList()<<"编号"<<"名称"<<"数量"<<"单价"<<"供应商家"<<"负责人"<<"入库时间"<<"出库时间"<<"备注");

            // 调用函数将数据表中数据再次展示到表格控件当中
            InitTableWidgetFunc();
        }

    }
    else
    {
        return;
    }
}

// 商品入库
#include "goodswarehousing.h"
void MainWidget::on_pushButton_GoodsWareHousing_clicked()
{
    GoodsWarehousing *gwdlg=new GoodsWarehousing();
    gwdlg->show();
}

// 商品出库
#include "goodsdelivery.h"
void MainWidget::on_pushButton_GoodsDelivery_clicked()
{
    GoodsDelivery *gddlg=new GoodsDelivery();
    gddlg->show();

}

// 导出数据
#include <QFileDialog>
#include <QAxObject>
#include <QDesktopServices>

// 该函数功能：导出表格控件数据，直接生成Excel文件保存
void MainWidget::on_pushButton_LoadData_clicked()
{
    // 保存文件扩展名为：.xls
    QDateTime time;
    QString strTemp;

    // 1：获取当前系统当时作为文件名称进行保存
    time=QDateTime::currentDateTime();
    strTemp=time.toString("yyyy-MM-dd-hhmmss");
    // QMessageBox::information(this,"测试",strTemp);

    // 2：应用文件对话框来保存要导出文件名称(设置保存的文件名称)及数据信息
    QString strFileName=QFileDialog::getSaveFileName(this,tr("Excel Files"),QString("./%1%2.xls").arg(strTemp).arg("_kcgl"),tr("Excel Files(*.xls)"));
    // QMessageBox::information(this,"测试",strFileName);

    // 3：处理工作簿
    if(strFileName!=NULL)
    {
        QAxObject *excel=new QAxObject;
        if(excel->setControl("Excel.Application"));
        {
            excel->dynamicCall("SetVisible (bool Visible)",false);
            excel->setProperty("DisplayAlerts",false);

            QAxObject *workbooks=excel->querySubObject("WorkBooks"); // 获得工作簿集合
            workbooks->dynamicCall("Add"); // 创建一个工作簿
            QAxObject *workbook=excel->querySubObject("ActiveWorkBook"); // 获得当前工作簿
            QAxObject *worksheet=workbook->querySubObject("Worksheets(int)",1);
            QAxObject *cell;

            // 1：添加Excel文件表头数据
            for(int i=1;i<=ui->tableWidget_ListData->columnCount();i++)
            {
                cell=worksheet->querySubObject("Cells(int,int)",1,i);
                cell->setProperty("RowHeight",25);
                cell->dynamicCall("SetValue(const QString&)",ui->tableWidget_ListData->horizontalHeaderItem(i-1)->data(0).toString());
            }

            // 2：将表格数据保存到Excel文件当中
            for(int j=2;j<=ui->tableWidget_ListData->rowCount()+1;j++)
            {
                for(int k=1;k<=ui->tableWidget_ListData->columnCount();k++)
                {
                    cell=worksheet->querySubObject("Cells(int,int)",j,k);
                    cell->dynamicCall("SetValue(const QString&",ui->tableWidget_ListData->item(j-2,k-1)->text()+"\t");

                }
            }

            // 3：将刚才创建的Excel文件直接保存到指定的目录下
            workbook->dynamicCall("SaveAs(const QString&)",QDir::toNativeSeparators(strFileName)); // 保存到strFileName
            workbook->dynamicCall("Close()");
            excel->dynamicCall("Quit()");
            delete excel;
            excel=NULL;
        }
    }
}

// 数据汇总 备份数据
#include "datasum.h"

void MainWidget::on_pushButton_DataSummary_clicked()
{
    // 建议大家自己动手
    DataSum *dsdlg=new DataSum();
    dsdlg->show();
}


void MainWidget::TableWidgetListDataFunc() // tablewidget表格控件初始化操作
{
    // 编号 名称 数量 单价 供应商家 负责人 入库时间 出库时间 备注

    // 设置表格控件200行9列
    ui->tableWidget_ListData->setColumnCount(9);
    ui->tableWidget_ListData->setRowCount(200);

    // 设置表格控件字体大小
    ui->tableWidget_ListData->setFont(QFont("宋体",13));

    // 设置表格控件标题（表头）
    ui->tableWidget_ListData->setHorizontalHeaderLabels(QStringList()<<"编号"<<"名称"<<"数量"<<"单价"<<"供应商家"<<"负责人"<<"入库时间"<<"出库时间"<<"备注");
}

void MainWidget::InitTableWidgetFunc() // 初始化表格控件列宽度
{
    ui->tableWidget_ListData->setColumnWidth(0,80);
    ui->tableWidget_ListData->setColumnWidth(1,150);
    ui->tableWidget_ListData->setColumnWidth(2,80);
    ui->tableWidget_ListData->setColumnWidth(3,80);
    ui->tableWidget_ListData->setColumnWidth(4,300);
    ui->tableWidget_ListData->setColumnWidth(5,80);
    ui->tableWidget_ListData->setColumnWidth(6,250);
    ui->tableWidget_ListData->setColumnWidth(7,250);
    ui->tableWidget_ListData->setColumnWidth(8,180);

    // SQL查询语句
    QSqlQuery  sqlquery;
    sqlquery.exec("select *from commoditydatatable");

    int i=0;
    while(sqlquery.next())
    {
        // 将数据库里面的数据表读取出来转换成对应的字符串,赋给对应字符串变量
        QString strId=sqlquery.value(0).toString();
        QString strName=sqlquery.value(1).toString();
        QString strAmoumt=sqlquery.value(2).toString();
        QString strUnitPrice=sqlquery.value(3).toString();
        QString strSupplier=sqlquery.value(4).toString();
        QString strDirector=sqlquery.value(5).toString();
        QString strWareHouseTime=sqlquery.value(6).toString();
        QString strDeliveryTime=sqlquery.value(7).toString();
        QString strRemarks=sqlquery.value(8).toString();

        // 将字符串显示到表格控件当中
        ui->tableWidget_ListData->setItem(i,0,new QTableWidgetItem(strId));
        ui->tableWidget_ListData->setItem(i,1,new QTableWidgetItem(strName));
        ui->tableWidget_ListData->setItem(i,2,new QTableWidgetItem(strAmoumt));
        ui->tableWidget_ListData->setItem(i,3,new QTableWidgetItem(strUnitPrice));
        ui->tableWidget_ListData->setItem(i,4,new QTableWidgetItem(strSupplier));
        ui->tableWidget_ListData->setItem(i,5,new QTableWidgetItem(strDirector));
        ui->tableWidget_ListData->setItem(i,6,new QTableWidgetItem(strWareHouseTime));
        ui->tableWidget_ListData->setItem(i,7,new QTableWidgetItem(strDeliveryTime));
        ui->tableWidget_ListData->setItem(i,8,new QTableWidgetItem(strRemarks));

        i++;
    }
}
