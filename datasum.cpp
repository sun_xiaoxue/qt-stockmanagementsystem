﻿#include "datasum.h"
#include "ui_datasum.h"

DataSum::DataSum(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DataSum)
{
    ui->setupUi(this);

    // 禁用窗口最大化按钮
    setWindowFlags(windowFlags()&~Qt::WindowMaximizeButtonHint);

    // 禁止用户拖拉窗口改变窗口大小
    setFixedSize(this->width(),this->height());

    // 专门设计一个函数，初始化表格控件
    TableWidgetListDataFunc();


}

DataSum::~DataSum()
{
    delete ui;
}


void DataSum::TableWidgetListDataFunc() // 初始化表格控件
{
    // 商品编号 商品名称 商品数量 商品单价 总价 供应商家 负责人 入库时间 出库时间 备注

    // 设置表格200行10列
    ui->tableWidget_ListData->setColumnCount(10);
    ui->tableWidget_ListData->setRowCount(200);

    // 自定义表格控件的字体大小
    ui->tableWidget_ListData->setFont(QFont("宋体",12));

    // 设置表格控件的表头（列的标题）
    ui->tableWidget_ListData->setHorizontalHeaderLabels(QStringList()<<"商品编号"<<"商品名称"<<"商品数量"<<"商品单价"<<"商品总价"<<"供应商家"<<"负责人"<<"入库时间"<<"出库时间"<<"备注");

}

void DataSum::on_pushButton_DataSum_clicked()
{
    ui->tableWidget_ListData->setColumnWidth(0,80);
    ui->tableWidget_ListData->setColumnWidth(1,100);
    ui->tableWidget_ListData->setColumnWidth(2,80);
    ui->tableWidget_ListData->setColumnWidth(3,80);
    ui->tableWidget_ListData->setColumnWidth(4,100);

    ui->tableWidget_ListData->setColumnWidth(5,300);
    ui->tableWidget_ListData->setColumnWidth(6,80);
    ui->tableWidget_ListData->setColumnWidth(7,200);
    ui->tableWidget_ListData->setColumnWidth(8,200);
    ui->tableWidget_ListData->setColumnWidth(9,200);

    int i=0;
    QSqlQuery sqlquery;
    sqlquery.exec("select *from commoditydatatable");

    while(sqlquery.next())
    {
        QString StrId=sqlquery.value(0).toString();
        QString StrName=sqlquery.value(1).toString();
        QString StrAmount=sqlquery.value(2).toString();
        QString StrUnitPrice=sqlquery.value(3).toString();

        // 将商品数量 * 商品单价 = 商品总价
        double dSum=StrAmount.toDouble()*StrUnitPrice.toDouble();
        QString StrSum=QString("%1").arg(dSum);

        QString StrSupplier=sqlquery.value(4).toString();
        QString StrDirector=sqlquery.value(5).toString();
        QString StrWarehousingTime=sqlquery.value(6).toString();
        QString StrDeliveryTime=sqlquery.value(7).toString();
        QString StrRemarks=sqlquery.value(8).toString();

        ui->tableWidget_ListData->setItem(i,0,new QTableWidgetItem(StrId));
        ui->tableWidget_ListData->setItem(i,1,new QTableWidgetItem(StrName));
        ui->tableWidget_ListData->setItem(i,2,new QTableWidgetItem(StrAmount));
        ui->tableWidget_ListData->setItem(i,3,new QTableWidgetItem(StrUnitPrice));

        // 显示商品总价
        ui->tableWidget_ListData->setItem(i,4,new QTableWidgetItem(StrSum));

        ui->tableWidget_ListData->setItem(i,5,new QTableWidgetItem(StrSupplier));
        ui->tableWidget_ListData->setItem(i,6,new QTableWidgetItem(StrDirector));
        ui->tableWidget_ListData->setItem(i,7,new QTableWidgetItem(StrWarehousingTime));
        ui->tableWidget_ListData->setItem(i,8,new QTableWidgetItem(StrDeliveryTime));
        ui->tableWidget_ListData->setItem(i,9,new QTableWidgetItem(StrRemarks));

        i++;
    }
}

void DataSum::on_pushButton_InputData_clicked()
{

}

void DataSum::on_pushButton_OutputData_clicked()
{

}

void DataSum::on_pushButton_DataBackups_clicked()
{

}
































