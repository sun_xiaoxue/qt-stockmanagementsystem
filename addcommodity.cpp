﻿#include "addcommodity.h"
#include "ui_addcommodity.h"

AddCommodity::AddCommodity(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddCommodity)
{
    ui->setupUi(this);

    // 禁止窗口最大化按钮
    setWindowFlags(windowFlags()& ~Qt::WindowMaximizeButtonHint);

    // 禁止用户拖拉窗口来改变尺寸的大小
    setFixedSize(this->width(),this->height());

    // 光标定位（焦点）
    ui->lineEdit_Id->setFocus();

}

AddCommodity::~AddCommodity()
{
    delete ui;
}

void AddCommodity::on_pushButton_Exit_clicked()
{
    close();
}


void AddCommodity::on_pushButton_Add_clicked()
{
    QSqlQuery sqlResult;
    QString strId,strName,strAmount,strUnitPrice,strSupplier,strDirector,strInputTime,strOutputTime,strRemarks;

    strId=ui->lineEdit_Id->text();
    strName=ui->lineEdit_Name->text();
    strAmount=ui->lineEdit_Amount->text();
    strUnitPrice=ui->lineEdit_UnitPrice->text();
    strSupplier=ui->lineEdit_Supplier->text();
    strDirector=ui->lineEdit_Director->text();

    QDateTime datetime=QDateTime::currentDateTime();
    QString strnowinputtime=datetime.toString("yyyy-MM-dd HH:mm:ss");

    strInputTime=strnowinputtime;
    strOutputTime=strnowinputtime;

    strRemarks=ui->textEdit_Remarks->toPlainText();


    if(ui->lineEdit_Id->text().isEmpty())
    {
        // 字符转换我们前面已经讲过啦，这个大家自己去完成，不会再和我沟通就可以。
        QMessageBox::critical(this,"Tips","新增商品编号不能为空，请重新输入？");
        ui->lineEdit_Id->setFocus();

        return;
    }

    QString sqlquery=QString("insert into commoditydatatable(StockId,StockName,StockAmount,StockUnitPrice,Supplier,Director,WareHousTime,DeliveryTime,Remarks) "
                             "values('%1','%2','%3','%4','%5','%6','%7','%8','%9')")
            .arg(strId).arg(strName).arg(strAmount).arg(strUnitPrice).arg(strSupplier).arg(strDirector).arg(strInputTime).arg(strOutputTime).arg(strRemarks);

    if(sqlResult.exec(sqlquery))
    {
        QMessageBox::information(this,"Tips","恭喜你，商品记录插入成功？");
    }
    else
    {
        QMessageBox::critical(this,"Tips","对不起商品记录插入失败，请重新检查？");
    }

}
