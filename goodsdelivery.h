﻿#ifndef GOODSDELIVERY_H
#define GOODSDELIVERY_H

#include <QWidget>


#pragma execution_character_set("utf-8");


// 数据库相关头文件
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>

#include <QtDebug>
#include <QMessageBox>
#include <QDateTime>


namespace Ui {
class GoodsDelivery;
}

class GoodsDelivery : public QWidget
{
    Q_OBJECT

public:
    explicit GoodsDelivery(QWidget *parent = nullptr);
    ~GoodsDelivery();

private slots:
    void on_pushButton_OutputGoods_clicked();

    void on_pushButton_Exit_clicked();

private:
    Ui::GoodsDelivery *ui;

public:
    void InitComboBoxFunc(); // 初始化Combo box控件


};

#endif // GOODSDELIVERY_H
